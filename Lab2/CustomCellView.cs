﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    class CustomCellView
    {
        public string SrcImage1 { get; set; }
        public string SrcImage2 { get; set; }
        public string LabelTeam1 { get; set; }
        public string LabelTeam2 { get; set; }
    }
}
